# DJANGO REST FRAMEWORK
A set of tutorials to take a tour about DJANGO REST FRAMEWORK.

0- [Quickstart](https://gitlab.com/cyaoyapi/rest_framework_tutorial/-/tree/feature_00_quickstart)

1- [Serialization](https://gitlab.com/cyaoyapi/rest_framework_tutorial/-/tree/feature_01_serialization)

2- [Requests and Responses](https://gitlab.com/cyaoyapi/rest_framework_tutorial/-/tree/feature_02_requests_and_responses)


3- [Class-based Views](https://gitlab.com/cyaoyapi/rest_framework_tutorial/-/tree/feature_03_class_based_views)

4- [Authentication and Permissions](https://gitlab.com/cyaoyapi/rest_framework_tutorial/-/tree/feature_04_authentification_and_permissions)

5- [Relationships and Hyperlinked APIs](https://gitlab.com/cyaoyapi/rest_framework_tutorial/-/tree/feature_05_relationships_and_hyperlinked_apis)

6- [Viewsets and Routers](https://gitlab.com/cyaoyapi/rest_framework_tutorial/-/tree/feature_06_viewsets_and_routers)
